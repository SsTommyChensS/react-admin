import { List, Datagrid, TextField, EmailField, ReferenceField, EditButton, SimpleForm, TextInput, Edit, ReferenceInput, Create } from "react-admin";
import { required } from "react-admin";
import { useRecordContext } from "react-admin";
//Get all posts

//Search filters: Search by title
const commentFilter = [
    <TextInput source="q" label="Search" alwaysOn/>,
];

export const CommentList = () => {
    return (
        <List filters={commentFilter}>
            <Datagrid>
                <TextField source="id"/>
                <ReferenceField source="postId" reference="posts"/>
                <TextField source="name"/>
                <EmailField source="email"/>
                <TextField source="body"/>
                
                <EditButton/>
            </Datagrid>
        </List>
    )
};

//Edit a post 
export const CommentEdit = () => {
    return (
        <Edit title={<CommentTitle/>}>
            <SimpleForm>
                <TextInput source="id" disabled/>
                <ReferenceField source="postId" reference="posts"/>
                <TextInput fullWidth source="name" validate={[required()]} variant="outlined" color="warning"/>
                <TextInput fullWidth source="email" validate={[required()]} variant="outlined" color="warning"/>
                <TextInput fullWidth source="body" validate={[required()]} variant="outlined" color="warning" multiline/>
            </SimpleForm>
        </Edit>
    )
};

export const PostCreate = () => {
    return (
        <Create>
            <SimpleForm>
                <ReferenceInput source="userId" reference="users"/>
                <TextInput source="title"/>
                <TextInput source="body" multiline row={5}/>
            </SimpleForm>
        </Create>

    )
};

//Change Title of the Post by using useRecordContext()
const CommentTitle = () => {
    const record = useRecordContext();     

    return (
        <span>Edit Comment {record ? `"${record.name}"` : ''}</span>
    )
};
