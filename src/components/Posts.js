import { List, Datagrid, TextField, ReferenceField, EditButton, SimpleForm, TextInput, Edit, ReferenceInput, Create } from "react-admin";
import { required } from "react-admin";
import { useRecordContext } from "react-admin";
//Get all posts

//Search filters: Search by title
const postFilter = [
    <TextInput source="q" label="Search title" alwaysOn/>,
    <ReferenceInput source="userId" label = "User" reference="users"/>
];

export const PostList = () => {
    return (
        <List filters={postFilter}>
            <Datagrid>
                <TextField source="id"/>
                <ReferenceField source="userId" reference="users" />
                <TextField source="title"/>
                <EditButton/>
            </Datagrid>
        </List>
    )
};

//Edit a post 
export const PostEdit = () => {
    return (
        <Edit title={<PostTitle/>}>
            <SimpleForm>
                <TextInput source="id" disabled/>
                <ReferenceInput source="userId" reference="users"/>
                <TextInput fullWidth source="title" validate={[required()]} variant="outlined" color="warning"/>
                <TextInput fullWidth source="body" validate={[required()]} variant="outlined" color="warning" multiline/>
            </SimpleForm>
        </Edit>
    )
};

export const PostCreate = () => {
    return (
        <Create>
            <SimpleForm>
                <ReferenceInput source="userId" reference="users"/>
                <TextInput source="title"/>
                <TextInput source="body" multiline row={5}/>
            </SimpleForm>
        </Create>

    )
};

//Change Title of the Post by using useRecordContext()
const PostTitle = () => {
    const record = useRecordContext();     

    return (
        <span>Edit Post {record ? `"${record.title}"` : ''}</span>
    )
};
