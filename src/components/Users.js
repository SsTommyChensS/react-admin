import { Datagrid, EmailField, List, TextField, UrlField, TextInput } from 'react-admin';

//Search filters (source="q") -> full search in records
const userFilter = [
    <TextInput source="q" label="Search" alwaysOn/>,
];
//List:  reads the query parameters from the URL, crafts an API call based on these parameters, and puts the result in a React context. It also builds a set of callbacks allowing child components to modify the list filters, pagination, and sorting.
export const UserList = () => {
    return (
        <List filters={userFilter}>
            <Datagrid rowClick="edit">
                <TextField source="id" />
                <TextField source="name" />
                <EmailField source="email" />
                <TextField source="phone" />
                <UrlField source="website" />
                <TextField source="company.name" />
            </Datagrid>
        </List>
    )
};

