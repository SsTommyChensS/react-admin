import { Admin, Resource, defaultTheme } from "react-admin";
import jsonServerProvider from "ra-data-json-server";

import  Dashboard  from "./components/Dashboard";
import { UserList } from "./components/Users";
import { PostList, PostEdit, PostCreate } from "./components/Posts";
import { CommentList, CommentEdit } from "./components/Comments";
//Sidebar Icon
//Post
import PostIcon from "@mui/icons-material/Book";
import UserIcon from "@mui/icons-material/Group";
import CommentIcon from '@mui/icons-material/Comment';
//í8nProvider
import { i18nProvider } from "./i18n/i18nProvider";

function App() {
  const api_url = 'https://jsonplaceholder.typicode.com';
  const dataProvider = jsonServerProvider(api_url);

  //Dark theme
  const theme = {
    ...defaultTheme,
    palette: {
      mode: 'dark'
    },
  }
  
  return (
    <Admin theme={theme} i18nProvider={i18nProvider} dataProvider={dataProvider} dashboard={Dashboard}>
      <Resource name="posts" list={PostList} edit={PostEdit} create={PostCreate} icon={PostIcon}/>
      <Resource name="comments" list={CommentList} edit={CommentEdit} icon={CommentIcon} recordRepresentation="title"/>
      <Resource name="users" list={UserList} recordRepresentation="name" icon={UserIcon}/>
    </Admin>
  )
}

export default App;