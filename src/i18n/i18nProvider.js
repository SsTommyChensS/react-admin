//This file provide multiple languages supporting for the admin dashboard
import polyglotI18nProvider from 'ra-i18n-polyglot';

import en from 'ra-language-english';
import fr from 'ra-language-french';
import vn from  'ra-language-vietnamese'

const translations = { en, fr, vn };

export const i18nProvider = polyglotI18nProvider(
    locale => translations[locale],
    'en', //default locale
    [
        { locale: 'en', name: 'English' },
        { locale: 'fr', name: 'Français'},
        { locale: 'vn', name: 'Việt Nam'}
    ]
);